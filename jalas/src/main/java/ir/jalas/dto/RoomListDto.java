package ir.jalas.dto;

import java.util.List;

public class RoomListDto {
	
	private List<Integer> availableRooms;
	
	public List<Integer> getAvailableRooms() {
		return availableRooms;
	}
}
