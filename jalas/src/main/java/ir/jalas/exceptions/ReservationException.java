package ir.jalas.exceptions;

import ir.jalas.client.model.HttpResponse;

public class ReservationException extends Exception {
	
	HttpResponse httpResponse;
	
	public ReservationException(HttpResponse httpResponse) {
		this.httpResponse = httpResponse;
	}
	
	public HttpResponse getHttpResponse() {
		return httpResponse;
	}
}
