package ir.jalas.model;

public class ReservationMessage {

	private String message;

	public ReservationMessage(String message) {
		this.message = message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
