package ir.jalas.model;

import java.util.ArrayList;
import java.util.List;

public class RoomList {
	
	private ArrayList<Room> rooms;
	
	public RoomList() {
		this.rooms = new ArrayList<Room>();
	}
	
	public RoomList(List<Room> rooms) {
		this.rooms = (ArrayList<Room>) rooms;
	}
	
	public void add(Room room) {
		this.rooms.add(room);
	}
	
	public ArrayList<Room> getRooms() {
		return rooms;
	}

}
