package ir.jalas.model;

public class ReservationPostBody {
	
	private String start;
	private String end;
	private String username;
	
	public String getStart() {
		return start;
	}
	
	public String getEnd() {
		return end;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setEnd(String end) {
		this.end = end;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setStart(String start) {
		this.start = start;
	}
}
