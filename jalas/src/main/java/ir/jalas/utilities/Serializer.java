package ir.jalas.utilities;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

import ir.jalas.exceptions.SerializeException;

public class Serializer {
	private static  ObjectMapper mapper = new ObjectMapper();
	public static String serialize(Object object) throws SerializeException {
        String result;
		try {
            result = mapper.writeValueAsString(object);
            return result;
        } catch (IOException e) {
            throw new SerializeException();
        }
    }
}
