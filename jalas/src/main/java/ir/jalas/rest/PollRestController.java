package ir.jalas.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/poll")
public class PollRestController {
    @RequestMapping(value = "/{pollId}", method = RequestMethod.GET)
    public ResponseEntity getPoll(@PathVariable(value="pollId") String pollId) throws Exception {
        return new ResponseEntity("", HttpStatus.OK);
    }
}
