package ir.jalas.rest;

import ir.jalas.client.HttpClient;
import ir.jalas.exceptions.ReservationException;
import ir.jalas.model.ReservationMessage;
import ir.jalas.model.ReservationPostBody;
import ir.jalas.model.RoomList;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/reservation")
public class ReservationRestController {
	
	@RequestMapping(value = "/availableRooms", method = RequestMethod.GET)
    public ResponseEntity getAvailableRooms(@RequestParam String start, @RequestParam String end) throws Exception {
		
		RoomList availableRooms = null;
		if ((start == null) || (end == null)) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
		try {
			availableRooms = HttpClient.fetchAavailableRooms(start, end);
		} catch (ReservationException e) {
            return new ResponseEntity<>(new ReservationMessage(e.getHttpResponse().getResponse()),
                    HttpStatus.INTERNAL_SERVER_ERROR);
		}
        return new ResponseEntity<>(availableRooms, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/reserve/room/{roomNumber}", method = RequestMethod.POST)
    public ResponseEntity reserveRooms(@PathVariable(value="roomNumber") String roomNumber,
    		@RequestBody ReservationPostBody reservationPostBody) throws Exception {
		
        if (roomNumber == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        ReservationMessage message = null;
        try {
            message = HttpClient.reserveRoom(reservationPostBody, roomNumber);
        } catch(ReservationException e) {
            return new ResponseEntity<>(new ReservationMessage(e.getHttpResponse().getResponse()),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
