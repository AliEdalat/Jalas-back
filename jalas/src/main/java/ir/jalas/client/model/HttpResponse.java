package ir.jalas.client.model;

public class HttpResponse {
	private String response;
    private Integer responseCode;

    public HttpResponse() {
        response = "";
        responseCode = 0;
    }

    public HttpResponse(String response, Integer responseCode) {
        this.response = response;
        this.responseCode = responseCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }
}
