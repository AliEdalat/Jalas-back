package ir.jalas.client;

import ir.jalas.client.model.HttpResponse;
import ir.jalas.exceptions.ReservationException;
import ir.jalas.exceptions.SerializeException;
import ir.jalas.model.ReservationMessage;
import ir.jalas.model.ReservationPostBody;
import ir.jalas.model.Room;
import ir.jalas.model.RoomList;
import ir.jalas.dto.RoomListDto;
import ir.jalas.utilities.Deserializer;
import ir.jalas.utilities.Serializer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class HttpClient {

    private static HttpResponse fetchRespose(HttpURLConnection con) {
        int responseCode = 0;
        try {
            responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(),  "UTF-8"));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return new HttpResponse(response.toString(), responseCode);
        } catch (Exception e){
            return new HttpResponse("", responseCode);
        }
    }
	
	private static HttpResponse get(String url) throws MalformedURLException {
        URL obj = new URL(url);
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
        } catch (Exception e) {
            e.printStackTrace();
            return new HttpResponse( "سرویس در دسترس نمی‌باشد.", 500);
        }
        return fetchRespose(con);
    }
	
	private static HttpResponse post(String url, String urlParameters) throws MalformedURLException {
        HttpResponse result = new HttpResponse();
        URL obj = new URL(url);
        byte[] postData = urlParameters.getBytes( StandardCharsets.UTF_8 );
        int postDataLength = postData.length;
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
            con.setDoOutput( true );
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Content-Length", Integer.toString( postDataLength ));
            try( DataOutputStream wr = new DataOutputStream( con.getOutputStream())) {
                wr.write( postData );
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new HttpResponse( "سرویس در دسترس نمی‌باشد.", 500);
        }
        return fetchRespose(con);
    }

    private static HttpResponse findMeaningfulMessageOfAvailableRooms(int code) {
        System.out.println(code);
        if (code == 400) {
            return new HttpResponse("Bad Request -- Invalid start or end date", code);
        } else if (code == 500) {
            return new HttpResponse("سیستم رزواسیون در حال حاضر پاسخگو نمی‌باشد.", code);
        }
        return null;
    }

    private static HttpResponse findMeaningfulMessageOfResevation(int code) {
        if(code == 400){
            return new HttpResponse("Bad Request -- Invalid start or end date, Required values not provided", code);
        } else if(code == 404) {
            return new HttpResponse("اتاقی با این شماره یافت نشد.", code);
        } else if (code == 500) {
            return new HttpResponse("سیستم رزواسیون در حال حاضر پاسخگو نمی‌باشد.", code);
        }
        return null;
    }

    public static RoomList fetchAavailableRooms(String start, String end) throws SerializeException,
            ReservationException, MalformedURLException {
        String url = String.format("http://213.233.176.40/available_rooms?start=%s&end=%s", start, end);
        System.out.println(url);
        HttpResponse response = get(url);
        System.out.println(response.getResponse());
        if (response.getResponseCode() != 200) {
        	throw new ReservationException(findMeaningfulMessageOfAvailableRooms(response.getResponseCode()));
        }
    	RoomListDto roomNumbers = Deserializer.deserialize(response.getResponse(), RoomListDto.class);
    	RoomList roomList = new RoomList();
    	for(Integer number : roomNumbers.getAvailableRooms()) {
    		roomList.add(new Room(number));
    	}
        return roomList;
    }
    
    public static ReservationMessage reserveRoom(ReservationPostBody postBody, String roomNumber) throws SerializeException, ReservationException, MalformedURLException {
        String url = String.format("http://213.233.176.40/rooms/%s/reserve", roomNumber);
        String body = Serializer.serialize(postBody);
        HttpResponse response = post(url, body);
        if (response.getResponseCode() != 200) {
        	throw new ReservationException(findMeaningfulMessageOfResevation(response.getResponseCode()));
        }
    	ReservationMessage message = Deserializer.deserialize(response.getResponse(), ReservationMessage.class);
        return message;
    }

}
